# Russian translations for iotas package.
# Copyright (C) 2022 iotas
# This file is distributed under the same license as the iotas package.
# Yaroslav Pronin <proninyaroslav@mail.ru>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: iotas\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-06-11 22:10+0300\n"
"PO-Revision-Date: 2022-06-11 22:10+0300\n"
"Last-Translator: Yaroslav Pronin <proninyaroslav@mail.ru>\n"
"Language-Team: Russian <gnu@d07.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:5
msgid "@NAME@"
msgstr "@NAME@"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:10
#: data/ui/about_dialog.ui.in:7
msgid "Simple note taking"
msgstr "Простое ведение заметок"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:12
msgid ""
"Note: It's fairly early days in development here, so please expect a few "
"rough edges."
msgstr ""
"Примечание. Это довольно ранние этапы разработки, поэтому, пожалуйста, "
"ожидайте возможные шероховатости."

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:13
msgid ""
"Iotas is a simple note taking app with mobile-first design and a focus on "
"sync with a Nextcloud Notes."
msgstr ""
"Iotas - это простое приложение для создания заметок с мобильным дизайном и "
"акцентом на синхронизацию с Nextcloud Notes."

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:14
msgid "Although consciously bare bones there are a few features"
msgstr "Хотя это сознательно примитивное приложение, есть несколько "
"особенностей"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:16
msgid "Sync. with Nextcloud Notes"
msgstr "Синхронизация с Nextcloud Notes"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:17
msgid "Offline note editing, syncing when back online"
msgstr ""
"Редактирование заметок в автономном режиме, синхронизация при "
"подключении к сети"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:18
msgid "Basic search"
msgstr "Базовый поиск"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:19
#: data/ui/index.ui:205
msgid "Favorites"
msgstr "Избранное"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:20
msgid "Follow system-wide dark style preference or make own choice"
msgstr ""
"Системная тема или ручной выбор тёмной или светлой темы"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:21
msgid "Ability to change font size"
msgstr "Возможность изменить размер шрифта"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:23
msgid "Чуть больше технических подробностей, для любителей подобных вещей"
msgstr ""
"Detalles un poco más técnicos, para los interesados en ese tipo de cosas"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:25
msgid ""
"Nextcloud Notes sync. is via the REST API, not WebDAV, which makes it snappy"
msgstr ""
"Синхронихация с Nextcloud Notes с помощью REST API, а не WebDAV, "
"что делает его быстрым"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:26
msgid "There's basic sync. conflict detection"
msgstr "Базовая система обнаружения конфликтов синронизации"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:27
msgid "Notes are constantly saved"
msgstr "Заметки постоянно сохраняются"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:28
msgid "Large note collections are partially loaded to quicken startup"
msgstr ""
"Большие коллекции заметок частично загружены для ускорения запуска"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:30
msgid "Current obvious feature omissions"
msgstr "Не реализованные функции"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:32
msgid "Spell checking"
msgstr "Проверка орфографии"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:33
msgid "Categories"
msgstr "Категории"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:34
msgid "Formatting"
msgstr "Форматирование"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:36
msgid ""
"Why \"Iotas\"? An iota is a little bit and this app is designed for jotting "
"down little things on little devices. Iota stems from the same Greek word as "
"jot and is commonly used in negative statements eg. \"not one iota of …\", "
"but we think the word has more to give. Maybe somebody will take note?"
msgstr ""
"Почему \"Iotas\"? Йота (Iota) - это немного, и это приложение предназначено для "
"записи мелочей на маленьких устройствах. Йота происходит от того же "
"греческого слова, что и йот, и обычно используется в отрицательных "
"утверждениях, например. \"ни на йоту …\", но мы думаем, что это слово может "
"дать больше. Может кто возьмёт на заметку?"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:41
msgid "Initial release."
msgstr "Первый выпуск"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:47
msgid "Mobile index"
msgstr "Мобильный список"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:51
msgid "Mobile editor"
msgstr "Мобильный редактор"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:55
msgid "Mobile index dark style"
msgstr "Мобильный список, тёмная тема"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:59
msgid "Desktop index"
msgstr "Десктопный список"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:63
msgid "Desktop index dark style"
msgstr "Десктопный список, тёмная тема"

#: data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in:71
msgid "Chris Heywood"
msgstr "Chris Heywood"

#. Translators: Iotas is the app name, do not translate
#: data/org.gnome.gitlab.cheywood.Iotas.desktop.in.in:4
msgid "Iotas"
msgstr "Iotas"

#: data/org.gnome.gitlab.cheywood.Iotas.desktop.in.in:5
msgid "Simple note taking with Nextcloud Notes"
msgstr "Простое ведение заметок с Nextcloud Notes"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.gitlab.cheywood.Iotas.desktop.in.in:13
msgid "notes;nextcloud;base;"
msgstr "notes;nextcloud;base;заметки;"

#. "Iotas" is the application name, do not translate
#: data/ui/about_dialog.ui.in:9
msgid "Learn more about Iotas"
msgstr "Узнайте больше о Iotas"

#. Add your name to the translator credits list
#: data/ui/about_dialog.ui.in:11
msgid "translator-credits"
msgstr "Yaroslav Pronin <proninyaroslav@mail.ru>"

#: data/ui/editor.ui:17 data/ui/keyboard_shortcuts_window.ui:37
msgid "Edit title"
msgstr "Редактировать заголовок"

#: data/ui/editor.ui:21 data/ui/index_row_popover.ui:17
msgid "Delete"
msgstr "Удалить"

#: data/ui/editor.ui:35
msgid "Back to Notes"
msgstr "Назад к заметкам"

#: data/ui/editor.ui:67
msgid "Revert changes"
msgstr "Отменить изменения"

#: data/ui/editor.ui:89
msgid "Apply changes"
msgstr "Применить изменения"

#: data/ui/empty_state.ui:10
msgid "Let's get started"
msgstr "Давайте начнём"

#: data/ui/empty_state.ui:41
msgid "Add new feeds via URL"
msgstr "Добавить новые каналы с помощью URL"

#: data/ui/empty_state.ui:51
msgid "Import an OPML file"
msgstr "Импорт OPML файла"

#: data/ui/index_row_popover.ui:7
msgid "Add Favorite"
msgstr "Добавить в избранное"

#: data/ui/index_row_popover.ui:12
msgid "Remove Favorite"
msgstr "Удалить из избранного"

#: data/ui/index.ui:24
msgid "Preferences"
msgstr "Параметры"

#: data/ui/index.ui:28
msgid "Keyboard Shortcuts"
msgstr "Комбинации клавиш"

#: data/ui/index.ui:32
msgid "About Iotas"
msgstr "О Iotas"

#: data/ui/index.ui:49
msgid "New note"
msgstr "Новая заметка"

#: data/ui/index.ui:56
msgid "Menu"
msgstr "Меню"

#: data/ui/index.ui:63 data/ui/keyboard_shortcuts_window.ui:20
msgid "Search"
msgstr "Поиск"

#: data/ui/index.ui:70
msgid "Sync. offline"
msgstr "Синхронизация оффлайн"

#: data/ui/index.ui:84
msgid "Back"
msgstr "Назад"

#: data/ui/index.ui:245
msgid "Today"
msgstr "Сегодня"

#: data/ui/index.ui:276
msgid "Yesterday"
msgstr "Вчера"

#: data/ui/index.ui:307
msgid "This Week"
msgstr "Эта неделя"

#: data/ui/index.ui:338
msgid "This Month"
msgstr "Этот месяце"

#: data/ui/index.ui:369
msgid "Last Month"
msgstr "Прошлый месяц"

#: data/ui/index.ui:399
msgid "Load The Rest"
msgstr "Загрузить остальные"

#: data/ui/index.ui:416
msgid "Rest"
msgstr "Остальные"

#: data/ui/keyboard_shortcuts_window.ui:10
msgid "Index"
msgstr "Список"

#: data/ui/keyboard_shortcuts_window.ui:14
msgid "Create new note"
msgstr "Создать новую заметку"

#: data/ui/keyboard_shortcuts_window.ui:33
msgid "Editor"
msgstr "Редактор"

#: data/ui/keyboard_shortcuts_window.ui:43
msgid "Delete note"
msgstr "Удалить заметку"

#: data/ui/keyboard_shortcuts_window.ui:49
msgid "Undo typing"
msgstr "Отменить ввод"

#: data/ui/keyboard_shortcuts_window.ui:55
msgid "Redo typing"
msgstr "Повторить ввод"

#: data/ui/keyboard_shortcuts_window.ui:61
msgid "Return to Index"
msgstr "Вернуться к списку"

#: data/ui/theme_selector.ui:25 data/ui/theme_selector.ui:27
msgid "Follow system style"
msgstr "Использовать системную тему"

#: data/ui/theme_selector.ui:42 data/ui/theme_selector.ui:44
msgid "Light style"
msgstr "Светлая тема"

#: data/ui/theme_selector.ui:59 data/ui/theme_selector.ui:61
msgid "Dark style"
msgstr "Тёмная тема"

#: data/ui/font_size_selector.ui:14 data/ui/font_size_selector.ui:17
msgid "Zoom out"
msgstr "Увеличить текст"

#: data/ui/font_size_selector.ui:32 data/ui/font_size_selector.ui:35
msgid "Zoom in"
msgstr "Уменьшить текст"

#: data/ui/nextcloud_login_window.ui:20
msgid "Cancel"
msgstr "Отмена"

#: data/ui/nextcloud_login_window.ui:28
msgid "Continue"
msgstr "Продолжить"

#: data/ui/nextcloud_login_window.ui:64
msgid ""
"Press Continue to provide your Nextcloud server address and login via a web "
"browser"
msgstr ""
"Нажмите кнопку «Продолжить», чтобы указать адрес своего сервера Nextcloud "
"и войти с помощью веб-браузера"

#: data/ui/nextcloud_login_window.ui:91
msgid ""
"The Secret Service could not be accessed for storing authentication details. "
"Ensure you have a provider such as gnome-keyring. A default keyring needs to "
"be setup, and that keyring unlocked. Most desktop environments will provide "
"this for you. Restart the app to try again."
msgstr ""
"Не удалось получить доступ к службе хранения данных аутентификации. "
"Убедитесь, что у вас есть провайдер, такой как gnome-keyring. Связка ключей "
"по умолчанию должна быть настроена и разблокирована. Большинство сред "
"рабочего стола обеспечивают это. Перезапустите приложение, чтобы повторить "
"попытку."

#: data/ui/nextcloud_login_window.ui:111
msgid "Nextcloud Server URL"
msgstr "URL сервера Nextcloud"

#: data/ui/nextcloud_login_window.ui:142
msgid "Waiting for completion of login in browser"
msgstr "Ожидание завершения входа в браузере"

#: data/ui/nextcloud_login_window.ui:174
msgid "Connection established with Nextcloud Notes"
msgstr "Установлено соединение с Nextcloud Notes"

#: data/ui/editor.ui:90
msgid "Apply"
msgstr "Применить"

#: data/ui/index.ui:12 data/ui/index.ui:156
msgid "Sync with Nextcloud Notes"
msgstr "Синхронизация с Nextcloud Notes"

#: data/ui/index.ui:17
msgid "Refresh"
msgstr "Обновить"

#. Iotas is the application name and shouldn't be translated
#: data/ui/index.ui:119
msgid "Welcome to Iotas"
msgstr "Добро пожаловать в Iotas"

#: data/ui/index.ui:137
msgid "Add a note"
msgstr "Добавить заметку"

#: data/ui/index.ui:170
msgid "Note list empty"
msgstr "Список заметок пуст"

#: data/ui/index.ui:176
msgid "Enter search term"
msgstr "Введите поисковый запрос"

#: data/ui/index.ui:182
msgid "No search results"
msgstr "Нет результатов поиска"

#: data/ui/preferences_window.ui:11
msgid "General"
msgstr "Общие"

#: data/ui/preferences_window.ui:14
msgid "Sync"
msgstr "Синхронизация"

#: data/ui/preferences_window.ui:17
msgid "Disconnect Nextcloud"
msgstr "Отсоединить Nextcloud"

#: data/ui/preferences_window.ui:18
msgid ""
"Signs out from Nextcloud Notes. All notes will be removed and the app will "
"quit."
msgstr ""
"Выход из Nextcloud Notes. Все заметки будут удалены, а приложение закроется."

#: data/ui/preferences_window.ui:41
msgid "Debug"
msgstr "Отладка"

#: data/ui/preferences_window.ui:44
msgid "Temporary Debug Functions"
msgstr "Временные функции отладки"

#: data/ui/preferences_window.ui:47
msgid "Reset Database"
msgstr "Сбросить базу данных"

#: data/ui/preferences_window.ui:48
msgid "Delete all notes from the local database. The app will quit."
msgstr "Удалить все заметки из локальной базы данных. Приложение будет закрыто."

#: data/ui/preferences_window.ui:65
msgid "Clear Sync Timestamp"
msgstr "Очистить временную метку синхронизации"

#: data/ui/preferences_window.ui:66
msgid "Forces a pull of all notes from the sync server."
msgstr "Принудительно извлекает все заметки с сервера синхронизации."

#: iotas/nextcloud_login_window.py:89
msgid "Finish"
msgstr "Финиш"

#: iotas/nextcloud_login_window.py:137
msgid "Failed to start login. Wrong address?"
msgstr "Не удалось открыть окно авторизации. Неверный адрес?"

#: iotas/sync_manager.py:368
msgid "SYNC CONFLICT - {}"
msgstr "КОНФЛИКТ СИНХРОНИЗАЦИИ - {}"

#: iotas/index.py:131 iotas/index.py:360
msgid "Loading"
msgstr "Загрузка"

#: iotas/index.py:233
msgid "Note deleted"
msgstr "Заметка удалена"

#: iotas/index.py:235
msgid "Undo"
msgstr "Отменить"

#: iotas/index.py:244
msgid "Sync conflict with note being edited"
msgstr "Конфликт синхронизации с редактируемой заметкой"

#: iotas/index.py:678
msgid "Syncing"
msgstr "Синхронизация"

#: iotas/index.py:688
msgid ""
"Failure accessing Secret Service. Ensure you have a provider like gnome-"
"keyring which has a default keyring setup that is unlocked."
msgstr ""
"Не удалось получить доступ к службе хранения данных аутентификации. "
"Убедитесь, что у вас есть провайдер, такой как gnome-keyring. Связка ключей "
"по умолчанию должна быть настроена и разблокирована."

#: iotas/index.py:692
msgid "OK"
msgstr "ОК"
