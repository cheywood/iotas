# Changelog

All notable changes will be documented in this file, the format for which is based on [Keep a Changelog](https://keepachangelog.com/). Higher level changes are tracked in the [AppStream](https://gitlab.gnome.org/World/iotas/-/blob/main/data/org.gnome.World.Iotas.metainfo.xml.in.in).

## [Unreleased]

### Added

- Ability to jump to other sections of the note being editor via an outline (#188)
  - Works in both editor and render
  - Outline generated on demand, no ongoing note structure overhead
  - Indentation levels are adaptive based on heading levels used in note
  - Can type to filter (then use `Alt + Enter` to open first match)
  - Attempts to use locale RTL detection to switch indentation to right side
  - Accessible via `Ctrl + J` and menu (j for jump)
- Shortcut help to open previous note

### Changed

- Clearer wording for onboarding help (#189)
- Consistency / HIG tweak to menu strings
- Use provided timestamp when launching from shell search results
- The editor closes if the note is remotely deleted

### Fixed

- Can't overwrite previous HTML exports


## [0.10.2] - 2025-02-27

### Added

- `Cache-Control` header to explicitly request no caching in an attempt to reduce false conflict
  issues (#186)


## [0.10.1] - 2025-02-19

### Added

- Ability to switch to the previous note
  - Attempts to retain cursor and scroll position
  - Accessible via `Ctrl + L`

### Changed

- Select exported file in file manager after export

### Fixed

- Cursor glitching when sitting in top or bottom margin when header bar auto hiding enabled
- Keyboard focus issue opening link dialog
- Keyboard focus issues after inserting table
- Handling of systems with no dictionaries by Balló György (@City-busz)


## [0.10.0] - 2025-01-23

### Added

- Confirmation dialogs for destructive actions
- Opening links via ctrl-click in the editor
- Shortcuts to move editor focus. `Alt + H` focuses the header bar, `Alt + F` the formatting bar and
  `Alt + E` the text view.

### Changed

- When not pinned the note list sidebar now behaves as an overlay
- Both the header and formatting bars in the editor are now overlays. This avoids having the note text
  move when the header bar hides.
- Using `Alt + ←` in the editor now moves the current word/selection left. This brings consistency
  with the other alt + arrow key shortcuts for moving text. `ESC` can be used to return to the note
  list.
- Switch editor headerbars to overlays. Avoids buffer text moving during show/hide.
- Switch note list sidebar to an overlay (when not pinned)
- Switch from revealer notifications to toasts
- Use `AdwButtonRow` for export dialog format selection
- Refresh sync onboarding dialog with recent widgets
- Align editor header bars with libadwaita style
- Improve high contrast appearance for the editor and markdown render. The default syntax theme 
  (Monochrome) provides the best high contrast experience.
- Note list focus ring no longer consistently shown after keyboard navigation use
- Disable editor font size and line length incremental change toasts
- Improve accessibility on a number of buttons, by adding tooltips or switching to text buttons
- String updates out of GNOME Circle review
- Use build in `StrEnum` if available by Arnaud Ferraris (@a-wai)
- Switch to target Python v3.11+
- Brazilian Portuguese translation update by Filipe Motta (@Tuba2)
- Chinese translation update by lumingzh (@flywater)
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Occitan translation update by Quentin Pagès (@Mejans)

### Fixed

- Preference combo rows not rendering in standard style


## [0.9.5] - 2024-11-15

### Changed

- Include charset header with HTML export when writing UTF8 (#178)


## [0.9.4] - 2024-10-29

### Fixed

- The formatting toolbar hiding the keyboard on mobile by Guido Günther (@guidog)


## [0.9.3] - 2024-10-15

### Changed

- Switch strikethrough shortcut to `Ctrl + Shift + X`
- Occitan translation update by Quentin Pagès (@Mejans)


## [0.9.2] - 2024-10-07

### Added

- Logic that attempts to reduce false sync conflicts on flaky connections (#170)
- Support for * and + as checkbox list markers

### Fixed

- Text such as what_is_a_code_span invoking italics in the editor (#74)


## [0.9.1] - 2024-09-25

### Changed

- Faster note list context switching. Switching between categories, switching in and out of search, 
  and searching itself are all faster.
- When inserting a new item in the middle of a simple ordered list the numbers for the remainder of
  the list are now incremented
- A longer timeout is now used for potentially larger syncs: initial import, when there hasn't been a
  sync in over two weeks and when the prune timestamp is reset
- Increase the total note count threshold above which notes are time filtered in the note list (ie.
  into "older notes")
- Increase initial window size

### Fixed

- Sidebar selection lost after item selected (when not configured pinned)


## [0.9.0] - 2024-09-18

### Added

- Markdown formatting assistance via a toolbar and keyboard shortcuts
- Typing as a trigger for the (optional) editor toolbars hiding
- Margin below cursor for when appending at the bottom of the editor
- Add atomic undo actions for editor actions: extending lists, tab indenting and checkbox toggling 
  (from render)
- Strikethrough markup highlighting in the editor
- Excerpt generator updated to show list bullet points

### Changed

- Improve editor toolbar hiding upon typing (more reliable and less delay)
- Menu theme switcher brought in sync with current styling
- Update a set of strings to header capitalisation
- Update editor headerbar style
- Stronger high contrast demarcation between header bar and editor
- Switch to v47 SDK
- Migrate to `AdwSpinner`
- Switch to target Python v3.9+
- Brazilian Portuguese translation update by Filipe Motta (@Tuba2)
- Chinese translation update by lumingzh (@flywater)
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)

### Fixed

- Assisted edits not having atomic undo actions (eg. indenting multiple lines)
- Inability to click in the editor directly under header bar
- Note list not scrolled to top changing categories
- Editor margins and body having a slightly different background colour
- Rare crash exiting note


## [0.8.2] - 2024-08-08

### Changed

- Make `Ctrl + F` refocus entry while searching in the editor
- Occitan translation update by Quentin Pagès (@Mejans)

### Fixed

- Issue editing note contents while searching


## [0.8.1] - 2024-08-07

### Changed

- Minor refactoring towards testability

### Fixed

- New notes flagging an extra sync after closing (#161)
- Button to show earlier notes in the note list can appear after adding a first note (#163)
- Math TeX equations not appearing in HTML exports
- Poor handling of a note conflict corner case


## [0.8.0] - 2024-06-06

### Added

- Ability to backup from CLI without closing open window (#151)
- `Ctrl + W` shortcut to close window
- `F10` shortcut to show menu
- Detection and communication of server app likely not installed (#153)
- Placeholder text in the editor search entry by Sabri Ünal (@sabriunal)
- Chinese translation by lumingzh (@flywater)

### Changed

- Attempt to avoid flash of previous markdown render (#31)
- Switch to proper approach (`ngettext`) for countable strings by Sabri Ünal (@sabriunal)
- Brazilian Portuguese translation update by Filipe Motta (@Tuba2)
- Czech translation update by Jiri Eischmann (@Sesivany)
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Italian translation update by Daniele Verducci (@penguin86)
- Occitan translation update by Quentin Pagès (@Mejans)
- Turkish translation update by Sabri Ünal (@sabriunal)


### Fixed

- Generic SSL errors not communicated during onboarding
- Being able to open multiple preferences dialogs via keyboard
- Directing users to browser too early, potentially resulting in them searching in their browser
  when eg. a URL typo had occurred
- Updating sync interval from setting
- Duplicate keyboard shortcut help items


## [0.2.14] - 2024-05-03

### Changed

- Brazilian Portuguese translation update by Filipe Motta (@Tuba2)
- Turkish translation update by Sabri Ünal (@sabriunal)

### Fixed

- Note list crash in search (#149)
- Some strings not marked translatable by Sabri Ünal (@sabriunal)


## [0.2.13] - 2024-04-19

### Added

- Explicit timeouts on network requests, configurable via `gsettings` with conservative ten second
  default
- Extended preference and `Ctrl + Shift + L` shortcut to toggle editor line length limit

### Changed

- Improve editor line length limiting model
- Align render view margins more closely with editor
- German translation update by Jürgen Benvenuti (@gastornis)
- Occitan translation update by Quentin Pagès (@Mejans)

### Fixed

- Ordering issue in the shell search provider (#144)
- Crash reauthenticating against Nextcloud (#146)
- Shell search provider not exiting when idle (#105)
- Jumps in the editor margin values


## [0.2.12] - 2024-04-02

### Added

- Ability to display bottom section of restricted note list via scroll overshoot and touch dragging

### Changed

- Considerably faster search in the note list (via FTS search index and only loading top of results
  for large sets)
- Editor search and replace paper cuts
- Restore automatically navigating to the first editor search match
- Updated Nextcloud onboarding button styles for consistency with eg. export dialog
- Brazilian Portuguese translation update by Filipe Motta (@Tuba2)
- Czech translation update by Jiri Eischmann (@Sesivany)
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Italian translation update by Daniele Verducci (@penguin86)
- Occitan translation update by Quentin Pagès (@Mejans)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)

### Fixed

- Ordering issue in the editor when replacing multiple times
- Current match being lost in the editor when opening replace
- Pressing enter not doing anything useful while searching in the editor
- Jumping between search matches in the editor not always scrolling all the way to the match
- Scroll position for the first markdown render in the session
- Editor minimum width slightly too wide for mobile
- Opening new notes in render view when _Open In Formatted View_ extended preference enabled (#142)
- `Ctrl + F` while searching in the editor interrupting current search
- Replace in the editor not being sequential
- Showing replace in the editor losing current search match
- Able to make selection in both the editor buffer and search entry simultaneously
- Older notes automatically being shown after sync onboarding
- Bug with shortcut handling when already searching
- Crash in exporter exception handler
- AppStream no longer supporting `translatable=no` flagging of strings by Sabri Ünal (@sabriunal)


## [0.2.11] - 2024-03-20

### Changed

- Allow typing while keyboard navigating the note list to append the current search term (once
  focus has moved away from the search entry)
- Improve unsynced changes marker in the editor
- Stop note list row title moving laterally for local changes marker
- Set links to break on words in render view, attempting to avoid overflow
- Attempt to retain cursor position when note being edited is updated from sync
- Remove extra spacing at start of note list row by Nokse (@nokse22)
- Update icon for sync changes notification
- Update to libadwaita v1.5, migrate to `AdwDialog`

### Fixed

- Selections being left in the note list sections using tab (#92)
- Flickering codeblocks in render view (#138)
- Need to relaunch to toggle render view availability
- Using preferences from the editor (via keyboard shortcut)



## [0.2.10] - 2024-03-13

### Fixed

- Text cursor placement issue resulting from problematic workaround (#139)


## [0.2.9] - 2024-03-09

### Added

- Exporting to PDF, ODT, HTML (and markdown). In preview, feedback encouraged.
- Focus mode (#129)
- Italian translation by Daniele Verducci (@penguin86)

### Changed

- Improve cold launch startup performance
- Make render view loading screen display earlier
- Update keyboard shortcuts help
- Brazilian Portuguese translation update by Filipe Motta (@Tuba2)
- Czech translation update by Jiri Eischmann (@Sesivany)
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Occitan translation update by Quentin Pagès (@Mejans)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)

### Fixed

- Alerts being created as a result of attempting to work around a toolkit issue (#135)
- Note list keyboard focus could be lost (#134)


## [0.2.8] - 2024-02-23

### Added

- Search and replace in the editor (#119)
- Search within markdown render
- Automatic editor headerbar hiding
- Basic extending of ordered lists (#109)
- Sidebar scaling for the desktop
- Feedback while loading render engine
- Ability to create a note from within another note via `Ctrl + N`, bringing any selection along as
  the new note's content
- `Alt + Enter` keyboard shortcut to open first search result in the note list
- Czech translation by Jiri Eischmann (@Sesivany)
- Occitan translation by Quentin Pagès (@Mejans)

### Changed

- Sync notifications in the note list are now only shown when there are changes
- Update and switch symbolic icons by David Lapshin (@daudix)
- Port preferences rows to `AdwSwitchRow` by Hari Rana (@TheEvilSkeleton)
- A build system future proofing tweak by Hari Rana (@TheEvilSkeleton)
- Remove header bar hiding keyboard shortcut
- Don't show revealer notification in the note list for every sync, only changes
- Use libadwaita defined colours for editor background, remove custom light and dark CSS
- Add `word-completion` input hint to the editor text view, which should help for mobile keyboards
- Brazilian Portuguese translation update by Filipe Motta (@Tuba2)
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)
- Turkish translation update by Sabri Ünal (@sabriunal)

### Fixed

- The shell search provider not closing when idle (#105)
- Wrong state sometimes retained clearing editor search
- WebKit process not terminated closing the note from render (when configured not to be held in
  memory)


## [0.2.7] - 2024-01-18

### Changed

- New unique identifier aligning with move into GNOME World
- Brazilian Portuguese translation by Filipe Motta (@Tuba2)
- German translation update by Jürgen Benvenuti (@gastornis)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)
- Turkish translation update by Sabri Ünal (@sabriunal)


## [0.2.6] - 2023-10-31

### Changed

- `Ctrl + F` when already looking at note list search results will focus entry box and select all
  text
- Improve error handling during Nextcloud Notes sign in

### Fixed

- Editor not scrolling for newlines (#104)


## [0.2.5] - 2023-10-12

### Added

- Searching of notes from GNOME Shell
- Adjustment of font size between fixed and proportional for the markdown render

### Changed

- Resync the editor margin background colours with Adwaita (#98)
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Turkish translation update by Sabri Ünal (@sabriunal)

### Fixed

- Sublists having insufficient vertical space in rendered view


## [0.2.4] - 2023-09-21

### Added

- The ability to automatically expand the sidebar on desktop (optional, enabled by default)
- An experiment which, after opting in, provides an extended set of preferences

### Changed

- Improvements for keyboard navigation in the sidebar
- Notifications are now shown for settings changed via keyboard shortcuts in the editor
- For the rendered markdown:
  - The system document font is now used (#91)
  - Images no longer have to finish downloading before the view is shown
  - Images wider than the window are now scaled to fit (#93)
  - Whether notes are opened in edit or view mode can be now be configured (using an extended
    preference) (#86)
  - Using the system monospace font can be configured (using an extended preference)
- Switch to SDK v45 (and libadwaita v1.4 widgets)
- Bump the default window size
- German translation update by Jürgen Benvenuti (@gastornis)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)
- Turkish translation update by Sabri Ünal (@sabriunal)

### Fixed

- An issue where the first search in a session could be very slow
- Two cases which could result in false sync conflicts
- A bug where _Load Older Notes_ would be shown in the note list for tiny collections (#95)


## [0.2.3] - 2023-09-04

### Added

- Editor line length adjustment via `Ctrl + ↑` and `Ctrl + ↓` (#79)
- Editor font size adjustment via `Ctrl + -` / `Ctrl + 0` / `Ctrl + +`
- Clearing note list context via `Ctrl + Delete` and `Ctrl + Backspace`
- Markdown list indentation via `Tab` and `Shift + Tab`

### Changed

- Improve editor search match visibility and show a match count (#41)
- Tune server sign in flow including URI schema being pre-populated into address input box (#51)
- Sync onboarding dialog now displayed until initial sync finishes
- Integrate initial transfer from server in batches
- Tweak link styling in the markdown render
- Trim unneeded files from `GResource` file by Arnaud Ferraris (@a-wai)
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)

### Fixed

- Broken keyboard navigation upwards from "older notes" listbox
- Markdown list continuation interacting with copy/paste and undo/redo
- Older notes always being populated to note list after initial sync
- Markdown list continuation bug resulting in double markers


## [0.2.2] - 2023-08-01

### Added

- Optional and experimental maths equation support into markdown rendering (#5)

### Changed

- More graceful handling of Nextcloud Notes sync issues (#69)
- Tidy context-click menu in the markdown render (#75)
- German translation update by Jürgen Benvenuti (@gastornis)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)

### Fixed

- A crash entering search under specific conditions


## [0.2.1] - 2023-07-16

### Fixed

- Crash adding a note after selecting a category in the sidebar (#73)


## [0.2.0] - 2023-07-15

### Added

- Ability to return to note list via mouse back button

### Changed

- Sizeable internal cleanup and translation improvement
- Rename "The Rest" to "Older Notes"
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)
- Turkish translation update by Sabri Ünal (@sabriunal)

### Fixed

- Category ordering in the sidebar and category selection
- CRLF line breaks not displaying correctly in the note list excerpts (#68)
- Crash long pressing a word on mobile with spell checking disabled (#70)
- Disabling spellcheck ignored until restart
- Version setting in backend schema update mechanism


## [0.1.16] - 2023-05-08

### Added

- Support for the read-only property from Nextcloud Notes
- Support to use the file extension preference from the server for backups
- Some styling for tables in the markdown render
- Nextcloud Notes API version check

### Changed

- Server offline indicator becomes a banner
- Improve naming of action to toggle rendered markdown view
- French translation update by Irénée Thirion (@rene-coty)
- Turkish translation update by Sabri Ünal (@sabriunal)


## [0.1.15] - 2023-05-03

### Fixed

- Markdown render task list display issues (including checked status in subtasks and extra spacing)


## [0.1.14] - 2023-04-30

### Added

- Accessing spelling suggestions via long touch on mobile
- Improved inline code block appearance in lists

### Changed

- Default to retaining WebKitGTK in memory between uses
- Switch to SDK v44

### Fixed

- Rendering of block level elements in task list items (#60)
- Long lines in code blocks overflowing their container in the rendered markdown


## [0.1.13] - 2023-04-11

### Fixed

- Crash deleting note from editor


## [0.1.12] - 2023-04-05

### Added

- Ability to select notes from the list then delete, favourite or change category
- Type to search in the note list (#49)
- Button to clear category, in category header bar

### Changed

- Don't update note last modified timestamp for category changes
- Note list keyboard focus tweaks to prevent unexpected scrolling
- French translation update by Irénée Thirion (@rene-coty)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)

### Fixed

- Sync not starting after manual unlocking of keyring (#54)
- Launching a second instance, from eg. the CLI, breaking the first (#53)
- Note list selected row borders not matching their section borders on mobile
- Cancelling note list category change via revert button
- Sidebar top level categories sometimes incorrectly being identified as sub-categories


## [0.1.11] - 2023-03-28

### Added

- Tree view for sidebar when sub-categories are in use
- Category labels in the note list
- Basic support for servers using self-signed certificates

### Changed

- Improve keyboard navigation in the sidebar
- Improve filtering in category dropdown (no longer only matching from the start)
- Substitute markdown check marks with special characters in note excerpts
- Account for header markdown when removing title from note excerpt
- Switch base language to American English and add British English translation
- Remove workaround when displaying the note list popup on touch devices
- Rename "All" to "All notes" (in sidebar and window title)
- Trim note list category label style options and consolidate label colour
- Reorder preferences to help dropdowns display on mobile
- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)
- Turkish translation update by Sabri Ünal (@sabriunal)

### Fixed

- Notes losing the category they're created in, when logged in
- Swipe back to note list interfering with cursor manipulation on touch devices
- Crash on platforms not providing the FreeDesktop.org setting via D-Bus
- Refresh menu item visible when not logged in


## [0.1.10] - 2023-03-09

### Added

- Initial support for categories (with more coming soon) (#2)
- Backup and restoration (via the CLI, for when not signed into Nextcloud)
- Reintegration of titles and categories sanitised by the server
- Local sanitisation of titles

### Changed

- Improve preferences layout
- French translation update by Irénée Thirion (@rene-coty)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)
- Turkish translation update by Sabri Ünal (@sabriunal)

### Fixed

- Icon background black under KDE (#44)
- Nextcloud signout showing when sync not established


## [0.1.9] - 2023-02-15

### Added

- Searching within notes
- A simple fullscreen mode (keyboard-only access for now)
- The ability to hide the headerbar in the editor for a minimised view (keyboard-only access for
  now)
- Preference to toggle use of monospace font
- Dutch translation by Heimen Stoffels (@Vistaus)

### Changed

- French translation update by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)
- Turkish translation update by Sabri Ünal (@sabriunal)

### Fixed

- Cursor not always placed at the beginning of the loaded note


## [0.1.8] - 2023-02-02

### Added

- Animated transition between mobile and desktop layouts in the note list by Adrien Plazas
  (@aplazas)
- French translation by Irénée Thirion (@rene-coty)
- German translation update by Jürgen Benvenuti (@gastornis)

### Changed

- Update about dialog to `AdwAboutWindow`

### Fixed

- Note list horizontal margin not retained at transition point between mobile and desktop layouts
  by Adrien Plazas (@aplazas)
- Wrong transition type between note list and editor


## [0.1.7] - 2022-11-10

### Added

- Font family for editor synced to system monospace font
- Spanish translation update by Óscar Fernández Díaz (@oscarfernandezdiaz)
- Turkish translation update by Sabri Ünal (@sabriunal)

### Changed

- Greatly improve hinting for translators


## [0.1.6] - 2022-11-07

### Added

- Accommodating for markdown headings when syncing first line of note into title
- Creating new note from CLI

### Fixed

- Under Phosh the keyboard appearing upon returning to the note list from the markdown render view
  (#33)
- Crash extending markdown lists containing links
- Crash copy and pasting lists with links
- Spelling mistakes flagged in markdown code blocks, links, etc (#32)



## [0.1.5] - 2022-11-02

### Added

- Initial markdown support, including:
  - Rendered markdown view
  - Syntax highlighting
  - Syntax themes
  - Task list support including toggling items from rendered view (which directly sync to server)
  - Scroll position matching between views (approximate)
  - Changes from server update into rendered view
  - Ability to disable markdown features
- Further markers into list continuation support

### Change

- Sync up with GtkSourceView dark background colour change
- Custom user agent for Nextcloud Notes API calls
- Extra logging for API calls

### Fixed

- Note could be shown twice in list (#20)


## [0.1.4] - 2022-08-11

### Added

- Startup notification
- German translation by Jürgen Benvenuti (@gastornis)
- Turkish translation by Sabri Ünal (@sabriunal)

### Changed

- Various misc. tidy

### Fixed

- Copy and paste merging problem with spelling enabled
- Task lists not continuing after checked task


## [0.1.3] - 2022-08-11

### Added

- Basic spell checking

### Changed 

- Update icon by Sam Hewitt (@snwh)
- To `main` branch, from `master`


## [0.1.2] - 2022-07-28

### Added

- Basic keyboard navigation in the note list (#3, #18)
- Help for missing Secret Service
- Russian translation by Yaroslav Pronin (@proninyaroslav)

### Fixed

- Case where no notes were shown on initial sync from server if all notes fell into "the rest"
  (older than the preceding month) by Yaroslav Pronin (@proninyaroslav) (#21)


## [0.1.1] - 2022-04-10

### Added 

- Continuation of basic markdown lists
- Spanish translation by Óscar Fernández Díaz (@oscarfernandezdiaz)

### Fixed

- Crash in sync RFC2822 date handling
- Title edits not getting immediately synced


## [0.1.0] - 2022-04-03

### Added 

- Initial release
