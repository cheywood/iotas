from abc import ABC, abstractmethod

from typing import Callable

from iotas.note import Note


class PdfExporter(ABC):
    """PDF export interface."""

    @abstractmethod
    def set_callbacks(self, finished_callback: Callable, error_callback: Callable) -> None:
        """Set functions to be called upon export result.

        :param Callable finished_callback: Finished callback
        :param Callable error_callback: Error callback
        """
        raise NotImplementedError()

    @abstractmethod
    def export(self, note: Note, location: str, keep_webkit_process: bool) -> None:
        """Export PDF of note.

        :param Note note: Note to export
        :param str location: Destination location
        """
        raise NotImplementedError()
