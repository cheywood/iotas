from gi.repository import Gio, Gtk

import logging

import iotas.config_manager


@Gtk.Template(resource_path="/org/gnome/World/Iotas/ui/font_size_selector.ui")
class FontSizeSelector(Gtk.Box):
    __gtype_name__ = "FontSizeSelector"

    _label = Gtk.Template.Child()

    VALID_SIZES = [
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        20,
        22,
        24,
        26,
        28,
        30,
        34,
        38,
    ]

    def __init__(self) -> None:
        super().__init__()
        self.__increase_action = None
        self.__decrease_action = None
        iotas.config_manager.settings.connect(
            f"changed::{iotas.config_manager.FONT_SIZE}",
            lambda _o, _v: self.__refresh_from_setting(),
        )

    def setup(self):
        self.__setup_actions()
        self.__refresh_from_setting()

    def increase(self) -> None:
        if not self.__increase_action.get_enabled():
            return

        previous_size = iotas.config_manager.get_font_size()
        ind = self.VALID_SIZES.index(previous_size)
        size = self.VALID_SIZES[ind + 1]
        iotas.config_manager.set_font_size(size)
        logging.info(f"Font size increased to {size}pt")

    def decrease(self) -> None:
        if not self.__decrease_action.get_enabled():
            return

        previous_size = iotas.config_manager.get_font_size()
        ind = self.VALID_SIZES.index(previous_size)
        size = self.VALID_SIZES[ind - 1]
        iotas.config_manager.set_font_size(size)
        logging.info(f"Font size decreased to {size}pt")

    def reset(self) -> None:
        previous_size = iotas.config_manager.get_font_size()
        default_size = iotas.config_manager.get_default_font_size()
        if previous_size != default_size:
            iotas.config_manager.set_font_size(default_size)
            logging.info(f"Font size reset (to {default_size}pt)")

    def __setup_actions(self) -> None:
        action_group = Gio.SimpleActionGroup.new()
        app = Gio.Application.get_default()

        action = Gio.SimpleAction.new("increase")
        action.connect("activate", lambda _o, _v: self.increase())
        action_group.add_action(action)
        self.__increase_action = action

        action = Gio.SimpleAction.new("decrease")
        action.connect("activate", lambda _o, _v: self.decrease())
        action_group.add_action(action)
        self.__decrease_action = action

        action = Gio.SimpleAction.new("reset")
        action.connect("activate", lambda _o, _v: self.reset())
        action_group.add_action(action)

        app.get_active_window().insert_action_group("font-size-selector", action_group)
        self.__action_group = action_group

    def __refresh_from_setting(self) -> None:
        size = iotas.config_manager.get_font_size()
        self._label.set_label("{}pt".format(size))
        ind = self.VALID_SIZES.index(size)
        self.__increase_action.set_enabled(ind + 1 < len(self.VALID_SIZES))
        self.__decrease_action.set_enabled(ind - 1 >= 0)
